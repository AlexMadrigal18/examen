﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KMART_CRUD_MVC_Examen.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KMART_CRUD_MVC_Examen.Controllers
{
    public class TiendaController : Controller
    {
        private readonly ApplicationDbContext _db;

        public TiendaController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {

            var displaydata = _db.Stock.ToList();
            return View(displaydata);
        }

        [HttpGet]

        public async Task<IActionResult> Index(String prodSearch)

        {
            ViewData["GetProductDetails"] = prodSearch;
            var productquery = from x in _db.Stock select x;
            if (!String.IsNullOrEmpty(prodSearch))
            {
                productquery = productquery.Where(x => x.KProd.Contains(prodSearch) || x.KCode.Contains(prodSearch));

            }
            return View(await productquery.AsNoTracking().ToListAsync());

        }
        public IActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Tienda nPr)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nPr);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(nPr);

        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getProductDetail = await _db.Stock.FindAsync(id);
            return View(getProductDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getProductDetail = await _db.Stock.FindAsync(id);
            return View(getProductDetail);
        }

        [HttpPost]

        public async Task<IActionResult> Edit(Tienda vProduct)

        {
            if (ModelState.IsValid)
            {
                _db.Update(vProduct);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(vProduct);

        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getProductDetails = await _db.Stock.FindAsync(id);
            return View(getProductDetails);
        }

        [HttpPost]

        public async Task<IActionResult> Delete(int id)

        {
            var getProductDetail = await _db.Stock.FindAsync(id);
            _db.Stock.Remove(getProductDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");

        }


    }
}
