﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KMART_CRUD_MVC_Examen.Models
{
    public class Tienda
    {
        [Key]
        public int KId { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del producto")]
        [Display(Name = "Nombre del producto")]
        public string KProd { get; set; }

        [Required(ErrorMessage = "Ingresa una descripcion del producto")]
        [Display(Name = "Descripcion")]
        public string KDesc { get; set; }

        [Required(ErrorMessage = "Ingresa el numero de unidades en Inventario")]
        [Display(Name = "Inventario")]
        [Range(1, 1000)]
        public int KStock { get; set; }

        [Required(ErrorMessage = "Ingresa un precio")]
        [Display(Name = "Precio")]
        public decimal KPrice { get; set; }


        [Required(ErrorMessage = "Ingresa un codigo de producto valido")]
        [Display(Name = "Codigo")]
        public string KCode { get; set; }
    }
}
