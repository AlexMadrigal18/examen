﻿using System.Web;
using System.Web.Mvc;

namespace KMART_REST_EXA
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
