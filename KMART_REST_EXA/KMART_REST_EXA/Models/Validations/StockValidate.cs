﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMART_REST_EXA.Models
{
    public class StockValidate
    {

        [MetadataType(typeof(Stock.MetaData))]
        public partial class Stock
        {
            sealed class MetaData
            {
                [Key]
                public int KId;

                [Required(ErrorMessage = "Ingresa Nombre de producto")]
                public string KProd;

                [Required(ErrorMessage = "Ingresa Una descripcion")]
                public string KDesc;

                [Required]
                [Range(1, 1000, ErrorMessage = "Ingresa una cantidad")]
                public Nullable<int> KStock;

                [Required]
                [Range(1, 10000, ErrorMessage = "Ingresa una cantidad")]
                public Nullable<decimal> KPrice;

                [Required(ErrorMessage = "Ingresa el codigo del producto")]
                public string KCode { get; set; }





            }
        }
    }
}