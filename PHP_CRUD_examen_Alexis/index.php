<?php
?>
<html>
<head>
	<title>Stock</title>
<script type="text/javascript"src="js/jquery-3.5.1.min.js">
</script>
<script type="text/javascript" src="js/mainFunctions.js">
</script>

 <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
</head>
<body >
	 <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>

    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
           <div class="container-fluid">
                <a class="navbar-brand" href="index.php"></a>
       
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link active" href="index.php">Pagina Principal</a></li>
                        <li><a class="nav-link" href="table.php">Administrar Stock</a></li>
                      
                    </ul>
                </div>
            </div>
        </nav>
    </header>


<div class="ulockd-home-slider">
        <div class="container-fluid">
            <div class="row">
                <div class="pogoSlider" id="js-main-slider">
                    <div class="pogoSlider-slide">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="slide_text">
                                        <h3><span class="theme_color">La Solucion Ideal</span> en equipos Electronicos</h3>
                                        <br>
                                        <h4>Contamos con una amplia gama<br> de productos modernos</h4>
                                        <br>
                                        <p>Lo mejor del mercado
                                         </p>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pogoSlider-slide" style="background-image:url(images/slider1.jpg);">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                <!-- .pogoSlider -->
            </div>
        </div>
    </div>
    <!-- End Banner -->


<footer class="footer-box">
        <div class="container">
            <div class="row">
               
                <div class="col-lg-12 white_fonts">
                    <h4 class="text-align">Contactanos</h4>
                </div>
                <div class="margin-top_30 col-md-8 offset-md-2 white_fonts">
                    <div class="row">
                        <div class="col-md-4">
                           
                                <img src="images/social1.png">
                            </div>
                            <div class="full white_fonts text_align_center">
                                <p>CDMX
                                    <br>Mexico</p>
                           
                        </div>
                        <div class="col-md-4">
                            <div class="full icon text_align_center">
                                <img src="images/social2.png">
                            </div>
                            
                                <p>electronicos@gmail.com
                                  </p>
                           
                        </div>
                        <div class="col-md-4">
                            <div class="full icon text_align_center">
                                <img src="images/social3.png">
                            </div>
                            
                                <p>018005763923
                                 </p>
                            
                        </div>
                    </div>
                </div>

          </footer>

  <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
