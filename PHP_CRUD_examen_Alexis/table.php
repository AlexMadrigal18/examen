<?php
?>
<html>
<head>
	<title>Stock</title>
<script type="text/javascript"src="js/jquery-3.5.1.min.js">
</script>
<script type="text/javascript" src="js/mainFunctions.js">
</script>

 <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
</head>
<body >
	 <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>

    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
           
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link active" href="index.php">Home</a></li>
                        <li><a class="nav-link" href="table.php">Administrar Stock</a></li>
                       
                    </ul>
                </div>
            </div>
        </nav>
    </header>



<div id = "lista_Stock">

    <?php 
include("php/list.php");
?>

   <link rel="stylesheet" type="text/css" href="css/tablestyle.css" />
<button type="button" class="boton_1" onclick="insert_new()";>Agregar Nuevo Producto</button>
</div>


<style type="text/css">
  .boton_1{
    text-decoration: none;
    padding: 2px;
    padding-left: 5px;
    padding-right: 5px;
    font-family: Arial;
    font-weight: 150;
    font-size: 15px;
    color: #FFFFFF;
    background-color: #0892fd;
    border-radius: 15px;
    border: 3px double #0892fd;
  }
  .boton_1:hover{
    opacity: 0.6;
    text-decoration: none;
  }
</style>



  <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
