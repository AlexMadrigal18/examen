<?php

include ("conexion.php");

$consulta = "SELECT 
				s_id,
				s_name,
				s_desc,
				s_stock,
				s_price
				FROM stocks";

$realiza_consulta = $conexion -> query($consulta) or die ("Error al realizar la consulta <br>". $conexion -> error);
?>
<table id = "listaStock" href="css/tablestyle.css">

	<tr>
		<th> ID</th>
		<th> Nombre</th>
		<th> Descripcion</th>
		<th> En Inventario</th>
		<th> Precio</th>
		<th colspan="2">Opciones</th>
	</tr>
<?php
	while( $array_query = $realiza_consulta -> fetch_row() ) 
	{
	echo	'<tr>';
		echo '<td>'.$array_query[0].'</td>';
		echo '<td>'.$array_query[1].'</td>';
		echo '<td>'.$array_query[2].'</td>';
		echo '<td>'.$array_query[3].'</td>';
		echo '<td>'.$array_query[4].'</td>';
		echo '<td> <button type="button" class="boton_1" onclick="delete_stock('.$array_query[0].');">';
		echo 'Eliminar</button></td>';
		echo '<td> <button type="button" class="boton_1" onclick="edit_stock('.$array_query[0].');">';
		echo 'Editar</button></td>';
		echo '</tr>';
	}
?>

<style type="text/css">
  .boton_1{
    text-decoration: none;
    padding: 2px;
    padding-left: 5px;
    padding-right: 5px;
    font-family: Arial;
    font-weight: 150;
    font-size: 15px;
    color: #FFFFFF;
    background-color: #0892fd;
    border-radius: 15px;
    border: 3px double #0892fd;
  }
  .boton_1:hover{
    opacity: 0.6;
    text-decoration: none;
  }
</style>
	<table>